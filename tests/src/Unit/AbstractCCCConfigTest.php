<?php

namespace Drupal\Tests\civiccookiecontrol\Unit;

use Drupal\civiccookiecontrol\CCCConfig\AbstractCCCConfig;
use Drupal\civiccookiecontrol\CookieCategoryInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Tests\UnitTestCase;

/**
 * Test description.
 *
 * @group civiccookiecontrol
 */
class AbstractCCCConfigTest extends UnitTestCase {

  public $configFactory;

  public $dateFormatter;

  public $cacheBackend;

  public $languageManager;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->configFactory = $this->createMock(ConfigFactoryInterface::class);
    $this->dateFormatter = $this->createMock(DateFormatterInterface::class);
    $this->cacheBackend = $this->createMock(CacheBackendInterface::class);
    $this->languageManager = $this->createMock(LanguageManagerInterface::class);
  }

  /** @test **/
  public function can_get_array_of_cookies_without_surrounding_spaces() {
    $cookieCategory = $this->createMock(CookieCategoryInterface::class);
    $cookieCategory->method('getCookies')->willReturn('cookie1, cookie2, __cookie3');
    $configStorage = $this->createMock(EntityStorageInterface::class);
    $configStorage->method('loadMultiple')->willReturn([$cookieCategory]);
    $entityTypeManager = $this->createMock(EntityTypeManager::class);
    $entityTypeManager->method('getStorage')
      ->with('cookiecategory')
      ->willReturn($configStorage);

    $abstractConfig = $this->getMockForAbstractClass(
      AbstractCCCConfig::class,
      [
        $this->configFactory,
        $entityTypeManager,
        $this->dateFormatter,
        $this->cacheBackend,
        $this->languageManager
      ]
    );
    $cookiesCategoryList = $abstractConfig->loadCookieCategoryList();

    $this->assertIsArray($cookiesCategoryList);
    $this->assertCount(1, $cookiesCategoryList);
    $this->assertArrayHasKey('cookies', $cookiesCategoryList[0]);
    $this->assertCount(3, $cookiesCategoryList[0]['cookies']);
    $this->assertEquals('cookie1', $cookiesCategoryList[0]['cookies'][0]);
    $this->assertNotEquals(' cookie1', $cookiesCategoryList[0]['cookies'][0]);
    $this->assertEquals('cookie2', $cookiesCategoryList[0]['cookies'][1]);
    $this->assertNotEquals(' cookie2', $cookiesCategoryList[0]['cookies'][1]);
    $this->assertEquals('__cookie3', $cookiesCategoryList[0]['cookies'][2]);
    $this->assertNotEquals(' __cookie3', $cookiesCategoryList[0]['cookies'][2]);
  }

  /** @test **/
  public function cookies_correctly_formatted_when_no_spaces_present_in_cookie_string() {
    $cookieCategory = $this->createMock(CookieCategoryInterface::class);
    $cookieCategory->method('getCookies')->willReturn('cookie1,cookie2,__cookie3');
    $configStorage = $this->createMock(EntityStorageInterface::class);
    $configStorage->method('loadMultiple')->willReturn([$cookieCategory]);
    $entityTypeManager = $this->createMock(EntityTypeManager::class);
    $entityTypeManager->method('getStorage')
      ->with('cookiecategory')
      ->willReturn($configStorage);

    $abstractConfig = $this->getMockForAbstractClass(
      AbstractCCCConfig::class,
      [
        $this->configFactory,
        $entityTypeManager,
        $this->dateFormatter,
        $this->cacheBackend,
        $this->languageManager
      ]
    );
    $cookiesCategoryList = $abstractConfig->loadCookieCategoryList();

    $this->assertIsArray($cookiesCategoryList);
    $this->assertCount(1, $cookiesCategoryList);
    $this->assertArrayHasKey('cookies', $cookiesCategoryList[0]);
    $this->assertCount(3, $cookiesCategoryList[0]['cookies']);
    $this->assertEquals('cookie1', $cookiesCategoryList[0]['cookies'][0]);
    $this->assertNotEquals(' cookie1', $cookiesCategoryList[0]['cookies'][0]);
    $this->assertEquals('cookie2', $cookiesCategoryList[0]['cookies'][1]);
    $this->assertNotEquals(' cookie2', $cookiesCategoryList[0]['cookies'][1]);
    $this->assertEquals('__cookie3', $cookiesCategoryList[0]['cookies'][2]);
    $this->assertNotEquals(' __cookie3', $cookiesCategoryList[0]['cookies'][2]);
  }

}
